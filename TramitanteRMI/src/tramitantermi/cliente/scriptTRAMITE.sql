USE [master]
GO

/****** Object:  Database [TramiteDocumentario]    Script Date: 27/05/2016 9:14:45 ******/
CREATE DATABASE [TramiteDocumentario]
 
ALTER DATABASE [TramiteDocumentario] SET COMPATIBILITY_LEVEL = 110
GO

IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [TramiteDocumentario].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO

ALTER DATABASE [TramiteDocumentario] SET ANSI_NULL_DEFAULT OFF 
GO

ALTER DATABASE [TramiteDocumentario] SET ANSI_NULLS OFF 
GO

ALTER DATABASE [TramiteDocumentario] SET ANSI_PADDING OFF 
GO

ALTER DATABASE [TramiteDocumentario] SET ANSI_WARNINGS OFF 
GO

ALTER DATABASE [TramiteDocumentario] SET ARITHABORT OFF 
GO

ALTER DATABASE [TramiteDocumentario] SET AUTO_CLOSE OFF 
GO

ALTER DATABASE [TramiteDocumentario] SET AUTO_CREATE_STATISTICS ON 
GO

ALTER DATABASE [TramiteDocumentario] SET AUTO_SHRINK OFF 
GO

ALTER DATABASE [TramiteDocumentario] SET AUTO_UPDATE_STATISTICS ON 
GO

ALTER DATABASE [TramiteDocumentario] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO

ALTER DATABASE [TramiteDocumentario] SET CURSOR_DEFAULT  GLOBAL 
GO

ALTER DATABASE [TramiteDocumentario] SET CONCAT_NULL_YIELDS_NULL OFF 
GO

ALTER DATABASE [TramiteDocumentario] SET NUMERIC_ROUNDABORT OFF 
GO

ALTER DATABASE [TramiteDocumentario] SET QUOTED_IDENTIFIER OFF 
GO

ALTER DATABASE [TramiteDocumentario] SET RECURSIVE_TRIGGERS OFF 
GO

ALTER DATABASE [TramiteDocumentario] SET  DISABLE_BROKER 
GO

ALTER DATABASE [TramiteDocumentario] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO

ALTER DATABASE [TramiteDocumentario] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO

ALTER DATABASE [TramiteDocumentario] SET TRUSTWORTHY OFF 
GO

ALTER DATABASE [TramiteDocumentario] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO

ALTER DATABASE [TramiteDocumentario] SET PARAMETERIZATION SIMPLE 
GO

ALTER DATABASE [TramiteDocumentario] SET READ_COMMITTED_SNAPSHOT OFF 
GO

ALTER DATABASE [TramiteDocumentario] SET HONOR_BROKER_PRIORITY OFF 
GO

ALTER DATABASE [TramiteDocumentario] SET RECOVERY SIMPLE 
GO

ALTER DATABASE [TramiteDocumentario] SET  MULTI_USER 
GO

ALTER DATABASE [TramiteDocumentario] SET PAGE_VERIFY CHECKSUM  
GO

ALTER DATABASE [TramiteDocumentario] SET DB_CHAINING OFF 
GO

ALTER DATABASE [TramiteDocumentario] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO

ALTER DATABASE [TramiteDocumentario] SET TARGET_RECOVERY_TIME = 0 SECONDS 
GO

ALTER DATABASE [TramiteDocumentario] SET  READ_WRITE 
GO

USE [TramiteDocumentario]
GO

/****** Object:  Table [dbo].[Correlativo]    Script Date: 27/05/2016 9:15:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[Correlativo](
	[NumExpediente] [nchar](10) NOT NULL
) ON [PRIMARY]

GO

USE [TramiteDocumentario]
GO

/****** Object:  Table [dbo].[Dependencias]    Script Date: 27/05/2016 9:16:19 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[Dependencias](
	[codDependencia] [char](5) NOT NULL,
	[nom_dependencia] [varchar](50) NOT NULL,
	[Descripicon] [varchar](200) NOT NULL,
	[UsuarioEncargado] [nchar](8) NOT NULL,
 CONSTRAINT [PK_Dependencias] PRIMARY KEY CLUSTERED 
(
	[codDependencia] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO


USE [TramiteDocumentario]
GO

/****** Object:  Table [dbo].[Tramite]    Script Date: 27/05/2016 9:16:43 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[Tramite](
	[NumeroExpediente] [nchar](10) NOT NULL,
	[FechaRecepcion] [smalldatetime] NOT NULL,
	[Procedencia] [varchar](50) NOT NULL,
	[Asunto] [varchar](300) NOT NULL,
	[Descripcion] [varchar](500) NOT NULL,
	[Area] [varchar](50) NOT NULL,
	[NumFolios] [int] NOT NULL,
	[Estado] [char](1) NOT NULL,
	[codUsuario] [nchar](8) NOT NULL,
	[TipodeTramite] [char](1) NOT NULL
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO

USE [TramiteDocumentario]
GO

/****** Object:  Table [dbo].[usuarioExterno]    Script Date: 27/05/2016 9:17:05 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[usuarioExterno](
	[DNI] [nchar](8) NOT NULL,
	[Nombre] [varchar](100) NOT NULL
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO

USE [TramiteDocumentario]
GO

/****** Object:  Table [dbo].[usuarioInterno]    Script Date: 27/05/2016 9:17:26 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[usuarioInterno](
	[DNI] [nchar](8) NOT NULL,
	[nom_usuario] [varchar](50) NOT NULL,
	[direccion] [varchar](50) NOT NULL,
	[telefono] [nchar](9) NOT NULL,
	[email] [varchar](50) NOT NULL,
	[login] [varchar](20) NULL,
	[clave] [varchar](20) NULL,
	[codDependencia] [char](5) NOT NULL,
 CONSTRAINT [PK_usuario] PRIMARY KEY CLUSTERED 
(
	[DNI] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO



USE [TramiteDocumentario]
GO

/****** Object:  StoredProcedure [dbo].[buscarUsuarioExterno]    Script Date: 27/05/2016 9:18:19 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
create PROCEDURE [dbo].[buscarUsuarioExterno] (@dni as nchar(8), @nombre as varchar(50) OUT)AS
BEGIN
	
	SET NOCOUNT ON;

	select @nombre=Nombre from UsuarioExterno where dni=@dni
    END

GO

USE [TramiteDocumentario]
GO

/****** Object:  StoredProcedure [dbo].[buscarUsuarioInterno]    Script Date: 27/05/2016 9:19:42 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[buscarUsuarioInterno] (@dni as nchar(8), @nombre as varchar(50) OUT, @area as varchar(50) OUT )AS
BEGIN
	
	SET NOCOUNT ON;

	select @nombre=nom_usuario, @area=nom_dependencia from vistausuinter where dni=@dni
    END

GO

USE [TramiteDocumentario]
GO

/****** Object:  StoredProcedure [dbo].[insertartramiteExterno]    Script Date: 27/05/2016 9:19:56 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE procedure [dbo].[insertartramiteExterno] 
(
 @asunto varchar(300),
 @desc varchar(500),
 @area varchar(50),
 @folios int,
 @codusu nchar(8),@res varchar(500) out) as
 begin 
 set nocount on;
  begin tran insertart
  begin try
    
    declare @cod as nchar(10)
	declare @fecha as smalldatetime
	select @fecha=convert(smalldatetime,getdate())
	select @cod= right('0000000000' + convert(varchar(10),NumExpediente+1),10)  from correlativo
	   INSERT INTO [Tramite]
           ([NumeroExpediente]
           ,[FechaRecepcion]
           ,[Procedencia]
           ,[Asunto]
           ,[Descripcion]
           ,[Area]
           ,[NumFolios]
           ,[Estado]
           ,[codUsuario]
           ,[TipodeTramite])
     VALUES
          ( @cod, @fecha,'MESAP',@asunto, @desc,@area,@folios,'0',@codusu,'2') 	   
		  UPDATE correlativo set NumExpediente=NumExpediente+1

		 
		  set @res ='registrada'
		  select @res as respuesta
     commit tran insertart
  end try
  begin catch
  set @res ='noseregistro'
  select @res as respuesta
  rollback tran insertart   
  end catch
end
GO

USE [TramiteDocumentario]
GO

/****** Object:  StoredProcedure [dbo].[insertartramiteInterno]    Script Date: 27/05/2016 9:20:31 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE procedure [dbo].[insertartramiteInterno] 
(
 @procedencia varchar(50),
 @asunto varchar(300),
 @desc varchar(500),
 @area varchar(50),
 @folios int,
 @codusu nchar(8),@RES varchar(500) out) as
 begin 
 set nocount on;
  begin tran insertart
  begin try
    
    declare @cod as nchar(10)
	declare @fecha as smalldatetime
	select @fecha=convert(smalldatetime,getdate())
	select @cod= right('0000000000' + convert(varchar(10),NumExpediente+1),10)  from correlativo
	   INSERT INTO [Tramite]
           ([NumeroExpediente]
           ,[FechaRecepcion]
           ,[Procedencia]
           ,[Asunto]
           ,[Descripcion]
           ,[Area]
           ,[NumFolios]
           ,[Estado]
           ,[codUsuario]
           ,[TipodeTramite])
     VALUES
          ( @cod, @fecha,@procedencia,@asunto, @desc,@area,@folios,'0',@codusu,'1') 	   
		  UPDATE correlativo set NumExpediente=NumExpediente+1

		 
		  set @res ='registrada'
		  select @res as respuesta
     commit tran insertart
  end try
  begin catch
  set @res ='noseregistro'
  select @res as respuesta
  rollback tran insertart   
  end catch
end
GO

USE [TramiteDocumentario]
GO

/****** Object:  StoredProcedure [dbo].[insertarusuarioE]    Script Date: 27/05/2016 9:20:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[insertarusuarioE] (@dni as nchar(8), @nombre as varchar(100),@res varchar(500) out)
AS
BEGIN
  set nocount on;
 begin tran insertart
  begin try
	INSERT INTO [dbo].[usuarioExterno]
           ([DNI]
           ,[Nombre])
     VALUES
           (@dni,@nombre)
		  set @res ='registrada'
		  select @res as respuesta
     commit tran insertart
  end try
  begin catch
  set @res ='noseregistro'
  select @res as respuesta
  rollback tran insertart   
  end catch
END

GO

