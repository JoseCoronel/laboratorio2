package ejemplo.servidor;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Jlazo
 */
import java.rmi.RemoteException;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;
public class BaseDatos extends java.rmi.server.UnicastRemoteObject implements InterfazRemota{
    
    public BaseDatos()throws java.rmi.RemoteException{}
    
    private static Connection con= null;
   
    public static Connection conexion() throws SQLException, ClassNotFoundException {
     if (con == null) {
        String connectionUrl = "jdbc:sqlserver://;databaseName=TramiteDocumentario;integratedSecurity=true;";      
    // Declaramos los sioguientes objetos
    
    try {
        //Establecemos la conexión
        Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
        con = DriverManager.getConnection(connectionUrl);            
    }
    catch (Exception e)
    {
        e.printStackTrace();
    }
  }
   return con;  }
    
       public static void cerrar() throws SQLException {
      if (con != null) {
         con.close();
      }
   }
  
    
    public String[] buscarI(String dni) throws RemoteException
    {   
        
        String a[]= new String [2];
        try{
            conexion();
       //llamado al procedimiento almancenados     
        CallableStatement cst = con.prepareCall("{call buscarUsuarioInterno (?,?,?)}");
        cst.setString(1, dni);
        cst.registerOutParameter(2, java.sql.Types.VARCHAR);
        cst.registerOutParameter(3, java.sql.Types.VARCHAR);
        cst.execute();
        
        a[0]=cst.getString(2);
        a[1]=cst.getString(3); 
        
        cerrar();
        }
        catch (Exception e)
        {
          e.printStackTrace();
        }
        return a;
    }


   public String areas() throws RemoteException
   {
       
       String a= null;
       ResultSet rs = null;
       Statement  st = null;      
      
       try {
           conexion();
              st = con.createStatement();
              rs = st.executeQuery ("SELECT nom_dependencia  FROM Dependencias");
              int i=0;
            while(rs.next()){
               String tmpStrObtenido = rs.getString("nom_dependencia");
               a=a+"-"+tmpStrObtenido;
           }
          cerrar();
         } catch (Exception e) {
             System.out.println("ERROR: failed to load HSQLDB JDBC driver.");
             e.printStackTrace();
         }        
        return a;
   }

   public void registrarI(String proc,String asunto,String desc,String area,int nunf,String dni) throws RemoteException
   {
   
        String a= "fracaso";
        try{
         conexion();
        CallableStatement cst = con.prepareCall("{call insertartramiteInterno(?,?,?,?,?,?,?)}");
        cst.setString(1, proc);
        cst.setString(2, asunto);
        cst.setString(3, desc);
        cst.setString(4, area);
        cst.setInt(5,nunf );
        cst.setString(6, dni);
        cst.registerOutParameter(7, java.sql.Types.VARCHAR);
        cst.execute();
        a=cst.getString(7); 
          System.out.printf(a);
        cerrar();
        }
          catch (Exception e)
    {
        e.printStackTrace();
        System.out.printf(a);
    }
   }
   
   public void registrarE(String asunto,String desc,String area,int nunf,String dni) throws RemoteException
   {
   
        String a= "fracasoalregistrar tramite externo";
        try{
         conexion();
        CallableStatement cst = con.prepareCall("{call insertartramiteExterno(?,?,?,?,?,?)}");
        cst.setString(1, asunto);
        cst.setString(2, desc);
        cst.setString(3, area);
        cst.setInt(4,nunf );
        cst.setString(5, dni);
        cst.registerOutParameter(6, java.sql.Types.VARCHAR);
        cst.execute();
        a=cst.getString(6); 
          System.out.printf(a);
        cerrar();
        }
          catch (Exception e)
    {
        e.printStackTrace();
        System.out.printf(a);
    }
   }

   public void registrarUsuarioE(String dni,String nombre) throws RemoteException
   {
   
        String a= "fracasoalregistrar";
        try{
        conexion();
        CallableStatement cst = con.prepareCall("{call insertarusuarioE(?,?,?)}");
        cst.setString(1, dni);
        cst.setString(2, nombre);
        cst.registerOutParameter(3, java.sql.Types.VARCHAR);
        cst.execute();
        a=cst.getString(3); 
          System.out.printf(a);
        cerrar();
        }
          catch (Exception e)
    {
        e.printStackTrace();
        System.out.printf(a);
    }   
   }
    @Override
    public String metodoRemoto() throws RemoteException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public String buscarE(String dni) throws RemoteException {
   
        
        String a= "";
        try{
            conexion();
        CallableStatement cst = con.prepareCall("{call buscarUsuarioExterno(?,?)}");
        cst.setString(1, dni);
        cst.registerOutParameter(2, java.sql.Types.VARCHAR);
        cst.execute();
        a=cst.getString(2);
        cerrar();
        }
          catch (Exception e)
    {   
        e.printStackTrace();
    }
       
        return a;
    }

}
