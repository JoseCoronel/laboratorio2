/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package ejemplo.servidor;

import java.rmi.RemoteException;

/**
 *
 * @author i5
 */
public interface InterfazRemota extends java.rmi.Remote{
    public String metodoRemoto() throws java.rmi.RemoteException;
    public String buscarE(String dni) throws java.rmi.RemoteException;
    public String[] buscarI(String dni) throws java.rmi.RemoteException;
     public String areas() throws java.rmi.RemoteException;
         public void registrarI(String proc,String asunto,String desc,String area,int nunf,String dni) throws java.rmi.RemoteException;
  public void registrarUsuarioE(String dni,String nombre) throws java.rmi.RemoteException;
     public void registrarE(String asunto,String desc,String area,int nunf,String dni) throws java.rmi.RemoteException;

}
