/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package singleton;

/**
 *
 * @author i5
 */
public class VentanaBusqueda {
    private static VentanaBusqueda instancia=null;
    private VentanaBusqueda(){
        
    }
    
    public static VentanaBusqueda getInstancia(){
        if(instancia==null) instancia=new VentanaBusqueda();
        return instancia;
    }
}
