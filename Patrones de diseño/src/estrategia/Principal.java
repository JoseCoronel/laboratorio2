package estrategia;
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author i5
 */
public class Principal {
    public static void main(String[] args) {
        Persona juan = new Persona("72727272","Pérez","Juan");
        juan.setRol(new Comprador());
        juan.negociar();
        juan.setRol(new Vendedor());
        juan.negociar();
    }
}
