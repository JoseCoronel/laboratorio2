/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package estrategia;

/**
 *
 * @author i5
 */
public class Persona {
    private String dni;
    private String apellidos;
    private String nombres;
    private Rol rol;

    public Persona(){
        
    }
    
    public Persona(String dni, String apellidos, String nombres){
        this.dni=dni;
        this.apellidos=apellidos;
        this.nombres=nombres;
    }
    public String getDni() {
        return dni;
    }

    public String getApellidos() {
        return apellidos;
    }

    public String getNombres() {
        return nombres;
    }
    
    public Rol getRol(){
        return rol;
    }

    public void setDni(String dni) {
        this.dni = dni;
    }

    public void setApellidos(String apellidos) {
        this.apellidos = apellidos;
    }

    public void setNombres(String nombres) {
        this.nombres = nombres;
    }
    
    public void setRol(Rol rol){
        this.rol=rol;
    }
            
    public void negociar(){
        rol.negociar();
    }
}
