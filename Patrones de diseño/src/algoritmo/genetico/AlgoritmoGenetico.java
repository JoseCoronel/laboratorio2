/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package algoritmo.genetico;

/**
 *
 * @author Moises
 */
public class AlgoritmoGenetico {
    
    public void generarPoblacionInicial(){
        System.out.println("Generando población inicial");
    }
    public void fitness(){
        System.out.println("Algoritmo fitness");
    }
    
    public void cruzamiento(){
        System.out.println("Algoritmo de cruzamiento");
    }
    
    public void seleccion(String alg){
        System.out.println("Algoritmo de selección");
        AlgoritmoSeleccion algoritmos = new AlgoritmoSeleccion();
        if(alg.equals("ruleta")){
            algoritmos.algoritmoRuleta();
        }
        else if(alg.equals("torneo")){
            algoritmos.algoritmoTorneo();
        }        
    }
    
    public void mutacion(){
        System.out.println("Algoritmo de mutación");
    }
    
    
}
