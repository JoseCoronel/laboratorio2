/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package algoritmo.genetico;

/**
 *
 * @author Moises
 */
public class Principal {
    public static void main(String[] args) {
        AlgoritmoGenetico ag = new AlgoritmoGenetico();
        ag.generarPoblacionInicial();
        ag.fitness();                
        ag.seleccion("ruleta");        
        ag.cruzamiento();
        ag.mutacion();
    }
}
