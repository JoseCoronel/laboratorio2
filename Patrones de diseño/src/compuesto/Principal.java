/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package compuesto;

/**
 *
 * @author i5
 */
public class Principal {

    public static void main(String[] args) {
        Archivo a1 = new Archivo("a1.txt", 10);
        Archivo a2 = new Archivo("a1.txt", 20);
        Archivo a3 = new Archivo("a1.txt", 30);
        Archivo a4 = new Archivo("a1.txt", 40);
        Archivo a5 = new Archivo("a1.txt", 50);
        Carpeta c1 = new Carpeta();
        c1.agregar(a1);
        c1.agregar(a2);
        Carpeta c2 = new Carpeta();
        c2.agregar(a3);
        c2.agregar(a4);
        Carpeta c3 = new Carpeta();
        c3.agregar(c1);
        c3.agregar(c2);
        System.out.println("Carpeta 1:" + c1.calcularEspacio());
        System.out.println("Carpeta 2:" + c2.calcularEspacio());
        System.out.println("Carpeta 3:" + c3.calcularEspacio());
    }
}
