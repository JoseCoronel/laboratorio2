/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package compuesto;

import java.util.ArrayList;

/**
 *
 * @author i5
 */
public class Carpeta implements Fichero{
    private ArrayList<Fichero> ficheros;
    
    public Carpeta(){
        ficheros = new ArrayList<Fichero>();
    }
    
    public void agregar(Fichero f){
        ficheros.add(f);
    }
    
    public double calcularEspacio(){
        double suma=0;
        for(int i=0;i<ficheros.size();i++){
            suma+=ficheros.get(i).calcularEspacio();
        }
        return suma;
    }
}
