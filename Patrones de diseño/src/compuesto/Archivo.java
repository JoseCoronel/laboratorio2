/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package compuesto;

/**
 *
 * @author i5
 */
public class Archivo implements Fichero{
    private String nombre;    
    private double espacio;     
    public Archivo(){this.espacio=0.0;}
    public Archivo(String n,double e){
        this.nombre=n;
        this.espacio=e;
    }
    public double getEspacio(){
        return espacio;
    }    
    public void setEspacio(double espacio) {
        this.espacio = espacio;
    }
    
    public double calcularEspacio(){
        return this.espacio;
    }
}
